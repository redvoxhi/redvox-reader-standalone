import redvox_reader_standalone as reader

# For API 0x800 data, the reader returns an instance of a "DecodedRedvoxFileApi800"
decoded_mic_80hz = reader.decode("wavs/rdvx_1000000004_1494538585_L_10.wav")
decoded_mic_800hz = reader.decode("wavs/rdvx_1307000006_1494538287_H_08.wav")
decoded_mic_8000hz = reader.decode("wavs/rdvx_1637165001_1494538228_A_07.wav")
decoded_bar = reader.decode("wavs/rdvx_1307000006_1494538135_B_08.wav")

# Each instance has methods that map 1-to-1 with the RedVox wave specification
# These methods also return the correct data types as specified in the speficifcation
# As an example, here is how you can access every field in our RedVox wave format
print decoded_mic_80hz.headerChunkId()
print decoded_mic_80hz.headerChunkSize()
print decoded_mic_80hz.headerChunkFormat()
print decoded_mic_80hz.headerSubChunkId()
print decoded_mic_80hz.headerSubChunkSize()
print decoded_mic_80hz.headerAudioFormat()
print decoded_mic_80hz.headerNumChannels()
print decoded_mic_80hz.headerSampleRate()
print decoded_mic_80hz.headerByteRate()
print decoded_mic_80hz.headerBlockAlign()
print decoded_mic_80hz.headerBitsPerSample()
print decoded_mic_80hz.rdvxChunkId()
print decoded_mic_80hz.rdvxChunkSize()
print decoded_mic_80hz.rdvxApiVersion()
print decoded_mic_80hz.rdvxServerRecvEpoch()
print decoded_mic_80hz.rdvxIdForVendor()
print decoded_mic_80hz.rdvxTruncatedUuid()
print decoded_mic_80hz.rdvxLatitude()
print decoded_mic_80hz.rdvxLongitude()
print decoded_mic_80hz.rdvxAltitude()
print decoded_mic_80hz.rdvxSpeed()
print decoded_mic_80hz.rdvxHorizontalAccuracy()
print decoded_mic_80hz.rdvxVerticalAccuracy()
print decoded_mic_80hz.rdvxCalibrationTrim()
print decoded_mic_80hz.rdvxTimeOfSolution()
print decoded_mic_80hz.rdvxFileStartMach()
print decoded_mic_80hz.rdvxFileStartEpoch()
print decoded_mic_80hz.rdvxNumMessageExchanges()
print decoded_mic_80hz.rdvxMessageExchanges()
print decoded_mic_80hz.rdvxSensorName()
print decoded_mic_80hz.rdvxDeviceName()
print decoded_mic_80hz.rdvxSyncServer()
print decoded_mic_80hz.rdvxDataServer()
print decoded_mic_80hz.dataChunkId()
print decoded_mic_80hz.dataChunkSize()
print decoded_mic_80hz.waveform()

# The superclass overrides the __str__ method, so you can print these decoded files directly...
print decoded_mic_80hz
print decoded_mic_800hz
print decoded_mic_8000hz
print decoded_bar