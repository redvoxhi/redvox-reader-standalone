#!/usr/bin/env bash

DIST_DIR="redvox-reader-standalone"
DIST_FILE=${DIST_DIR}.tar.gz
FILES=(
    "requirements.txt"
    "examples.py"
    "redvox_reader_standalone.py"
    "redvox_file_explorer_server.py"
    "wavs/"
    "docs/"
)

mkdir -p ${DIST_DIR}

for f in "${FILES[@]}"
do
    echo "Adding ${f}"
    cp -R ${f} ${DIST_DIR}
done

tar czf ${DIST_FILE} ${DIST_DIR}

rm -rf ${DIST_DIR}

echo "Distribution at:`pwd`/${DIST_FILE}"