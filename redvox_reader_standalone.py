import json
import struct
import numpy as np
import os

"""This module contains global variables and functions that specify the binary API for our audio data.

The main global variables in this module are lists of tuples which describe the binary format of the wav file that we
wish to decode.

Each tuple contains the following information: (byte offset, field name, np.dtype | (np.dtype, size))

It's important to note that np.dytpe only accepts fixed length sizes. This means the data field at the end of the format
is really only used to store the byte offset since its tuple can not be used in the creation of a numpy dtype.

Attributes
----------
mic_api_700 : list[(int, str, dtype)]
    Microphone API 700 definition
mic_api_800 : list[(int, str, dtype)]
    Microphone API 800 definition
bar_api_700 : list[(int, str, dtype)]
    Barometer API 700 definition
bar_api_800 : list[(int, str, dtype)]
    Barometer API 800 definition
API_OFFSET : int
    Byte offset of API field in all API versions
API_WIDTH : int
    Number of bytes that make up the API field
mic_formats : list[list[(int, str, dtype)]]
    List of valid microphone definitions
bar_formats : list[list[(int, str, dtype)]]
    List of valid barometer definitions
valid_apis : list[int]
    List of valid API versions that this framework supports
"""

mic_api_700 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),

    # Data chunk
    (696, 'dataChunkId', (np.str, 4)),
    (700, 'dataChunkSize', np.int32),
    (704, 'waveform', (np.int16, -1))
]
"""list[(int, string, dtype)]: Microphone API 700 dtype definition"""

mic_api_800 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),
    (696, 'rdvxDataServer', (np.str, 25)),

    # Data chunk
    (721, 'dataChunkId', (np.str, 4)),
    (725, 'dataChunkSize', np.int32),
    (729, 'waveform', (np.int16, -1))
]

bar_api_700 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),

    # Data chunk
    (696, 'dataChunkId', (np.str, 4)),
    (700, 'dataChunkSize', np.int32),
    (704, 'waveform', (np.float64, -1))
]

bar_api_800 = [
    # Header chunk
    (0, 'headerChunkId', (np.str, 4)),
    (4, 'headerChunkSize', np.int32),
    (8, 'headerChunkFormat', (np.str, 4)),
    (12, 'headerSubChunkId', (np.str, 4)),
    (16, 'headerSubChunkSize', np.int32),
    (20, 'headerAudioFormat', np.int16),
    (22, 'headerNumChannels', np.int16),
    (24, 'headerSampleRate', np.int32),
    (28, 'headerByteRate', np.int32),
    (32, 'headerBlockAlign', np.int16),
    (34, 'headerBitsPerSample', np.int16),

    # Redvox chunk
    (36, 'rdvxChunkId', (np.str, 4)),
    (40, 'rdvxChunkSize', np.int32),
    (44, 'rdvxApiVersion', np.int32),
    (48, 'rdvxServerRecvEpoch', np.int64),
    (56, 'rdvxIdForVendor', np.uint32),
    (60, 'rdvxTruncatedUuid', np.int32),
    (64, 'rdvxLatitude', np.float64),
    (72, 'rdvxLongitude', np.float64),
    (80, 'rdvxAltitude', np.float64),
    (88, 'rdvxSpeed', np.float64),
    (96, 'rdvxHorizontalAccuracy', np.float64),
    (104, 'rdvxVerticalAccuracy', np.float64),
    (112, 'rdvxCalibrationTrim', np.float64),
    (120, 'rdvxTimeOfSolution', np.int32),
    (124, 'rdvxFileStartMach', np.int64),
    (132, 'rdvxFileStartEpoch', np.int64),
    (140, 'rdvxNumMessageExchanges', np.int8),
    (141, 'rdvxMessageExchanges', (np.int64, 60)),
    (621, 'rdvxSensorName', (np.str, 30)),
    (651, 'rdvxDeviceName', (np.str, 20)),
    (671, 'rdvxSyncServer', (np.str, 25)),
    (696, 'rdvxDataServer', (np.str, 25)),

    # Data chunk
    (721, 'dataChunkId', (np.str, 4)),
    (725, 'dataChunkSize', np.int32),
    (729, 'waveform', (np.float64, -1))
]

API_OFFSET = 44
API_WIDTH = 4
mic_formats = [mic_api_700, mic_api_800]
bar_formats = [bar_api_700, bar_api_800]
valid_apis = {0x700, 0x800}


def get_data_offset(bin_format):
    """Returns the byte offset of the data field.

    Parameters
    ----------
    bin_format
        The binary format to extract the data offset from.

    Returns
    -------
    int
        The byte offset of the data field.

    """
    return get_offset_field("waveform", bin_format)


def get_offset_field(field, bin_format):
    """Returns the byte offset of the specified field.

    Parameters
    ----------
    field : str
        The field to extract the byte offset for.
    bin_format
        The binary format to extract the offset from.

    Returns
    -------
    int
       The byte offset of the selected field.

    """
    filtered = filter(lambda tuple3: tuple3[1] == field, bin_format)

    # Did not find key
    if len(filtered) == 0:
        return -1

    return filtered[0][0]


def get_dtype(bin_format):
    """Converts a binary format into a numpy dtype.

    Please note that this dtype will leave out the data field at the end of the binary format since dtypes can not
    include variable length types.

    Parameters
    ----------
    bin_format
        The selected binFormat to convert.

    Returns
    -------
    dtype
        Numpy dtype.

    """
    sans_data = bin_format[:-1]
    sans_offsets = map(lambda tuple3: (tuple3[1], tuple3[2]), sans_data)
    return np.dtype(sans_offsets)


def get_field_names(bin_format):
    """Returns a list of field names associated with a given binary format. Does not include the data field.

    Parameters
    ----------
    bin_format
        Binary format to extract field names from.

    Returns
    -------
    list[str]
        List of field names associated with a given binary format.

    """
    sans_data = bin_format[:-1]
    return map(lambda tuple3: tuple3[1], sans_data)


class BinDecoder:
    """An instance of a decoded binary wave file.

    Parameters
    ----------
    fields : dict{str, *}
        Contains a mapping of field name -> decoded value.

        Most fields in this instance are generated dynamically. That is, a numpy dtype and a specification of the
        encoded binary file are used to populate the fields of this instance. Each API version contains different fields
        so instead of creating a separate BinDecoder for each API, this class can decode any API and populate the fields
        dynamically in a dictionary.
    fname : str
        Filename of wave file to load and decode from disk. This can be empty or None if the file is instead loaded
        through a byte buffer.
    bin_format : list[tuple(int, str, dtype)]
        Binary specification to follow in order to decode the binary payload.
    header_only : boolean, optional
        When True, only load header information (metadata) and skip loading of binary waveform
    from_buffer : boolean, optional
        When True, decode instance from byte buffer rather than from a file. Also when True, byte_buffer must not be
        None.
    byte_buffer : list[byte], optional
        When from_buffer is True, this field must contain a buffer of bytes that is a multiple in size of the dtype
        generated by passing in the selected bin_format.
    """

    def __init__(self, fname, bin_format, header_only=False, from_buffer=False, byte_buffer=None):
        self.fields = {}
        self.bin_format = bin_format
        self.header_only = header_only
        self.fname = fname
        self.from_buffer = from_buffer
        self.byte_buffer = byte_buffer

        # Read in all of the data from the binary file using the schema defined by the numpy dtype object.
        # This will deserialize all fields except for the final waveform array.
        dtype = get_dtype(bin_format)
        if not from_buffer:
            fields_sans_data = np.fromfile(fname, dtype)
        else:
            buffer_sans_data = byte_buffer[:get_data_offset(bin_format)]
            fields_sans_data = np.frombuffer(buffer_sans_data, dtype)

        # Make all of the fields easily accessible in the fields list
        for field_name in get_field_names(bin_format):
            self.fields[field_name] = fields_sans_data[field_name][0]

        api = self.get("rdvxApiVersion")
        bits_per_sample = self.get("headerBitsPerSample")
        is_mic = bin_format in mic_formats
        is_bar = bin_format in bar_formats

        # TODO: Merge with get_waveform_buffer()
        def get_waveform_fs():
            """
            Fills self.fields['waveform'] with a list of ints for audio data of a list of floats for barometer data
            loaded from the filesystem.
            """
            # Get the waveform data
            if not header_only:
                fd = open(fname, 'rb')
                fd.seek(get_data_offset(bin_format), 0)  # File offset at data

                # API 700
                if api == 0x700:
                    if is_mic:
                        self.fields['waveform'] = np.fromfile(fd, "int" + str(bits_per_sample))
                    elif is_bar:
                        self.fields['waveform'] = np.fromfile(fd, "float" + str(bits_per_sample))
                    else:
                        print "Unknown bin_format " + bin_format

                # API 800
                elif api == 0x800:
                    if is_mic:
                        if bits_per_sample == 16:
                            self.fields['waveform'] = np.fromfile(fd, "int" + str(bits_per_sample))
                        elif bits_per_sample == 24:
                            # 24 bit read magic from https://github.com/scipy/scipy/issues/1930#issuecomment-112812537
                            data = np.fromfile(fd, "u1")
                            a = np.empty((len(data) / 3, 4), dtype='u1')
                            a[:, :3] = data.reshape((-1, 3))
                            a[:, 3:] = (a[:, 3 - 1:3] >> 7) * 255
                            data = a.view('<i4').reshape(a.shape[:-1])
                            self.fields['waveform'] = data
                        else:
                            print "Can not handle bits per sample = " + str(bits_per_sample)

                    elif is_bar:
                        self.fields['waveform'] = np.fromfile(fd, "float" + str(bits_per_sample))
                    else:
                        print "Unknown bin_format " + bin_format

                # Unknown API
                else:
                    print "Unknown API " + api

                fd.close()

        # TODO: merge with get_waveform_fs()
        def get_waveform_buffer():
            """
            Fills self.fields['waveform'] with a list of ints for audio data of a list of floats for barometer data
            loaded from a byte buffer.
            """
            # Get the waveform data
            if not header_only:
                data_offset = get_data_offset(bin_format)
                data_buffer = byte_buffer[data_offset:]
                # API 700
                if api == 0x700:
                    if is_mic:
                        self.fields['waveform'] = np.frombuffer(data_buffer, "int" + str(bits_per_sample))
                    elif is_bar:
                        self.fields['waveform'] = np.frombuffer(data_buffer, "float" + str(bits_per_sample))
                    else:
                        print "Unknown bin_format " + bin_format

                # API 800
                elif api == 0x800:
                    if is_mic:
                        if bits_per_sample == 16:
                            self.fields['waveform'] = np.frombuffer(data_buffer, "int" + str(bits_per_sample))
                        elif bits_per_sample == 24:
                            # 24 bit read magic from https://github.com/scipy/scipy/issues/1930#issuecomment-112812537
                            data = np.frombuffer(data_buffer, "u1")
                            a = np.empty((len(data) / 3, 4), dtype='u1')
                            a[:, :3] = data.reshape((-1, 3))
                            a[:, 3:] = (a[:, 3 - 1:3] >> 7) * 255
                            data = a.view('<i4').reshape(a.shape[:-1])
                            self.fields['waveform'] = data
                        else:
                            print "Can not handle bits per sample = " + str(bits_per_sample)

                    elif is_bar:
                        self.fields['waveform'] = np.frombuffer(data_buffer, "float" + str(bits_per_sample))
                    else:
                        print "Unknown bin_format " + bin_format

                # Unknown API
                else:
                    print "Unknown API " + api

        if not from_buffer:
            get_waveform_fs()
        else:
            get_waveform_buffer()

    def get_device_id(self):
        """Returns the device id of this decoded file.

        Returns
        -------
        int
            The device id of this decoded file.
        """
        return self.fields['rdvxIdForVendor']

    def get_app_time_us(self):
        """Returns the app time of this decoded file.

        Returns
        -------
        int
            The app time of this decoded file.
        """
        return self.fields['rdvxFileStartEpoch']

    def get_waveform(self):
        """Returns the waveform of this decoded file as a numpy array

        Returns
        -------
        np.array[int|float]
            Waveform of this decoded file as a numpy array.
        """
        return self.fields['waveform']

    def get(self, field_name):
        """Helper method to easily get the value of a field stored in this BinDecoder instance.


        Parameters
        ----------
        field_name : str


        Examples
        --------
        If you have an instance of a BinDecoder decoder, you could access the field "waveform" as follows:

            decoder.fields["waveform"]

        If you find the above slightly confusing, this utility method provides the following way of accessing the field
        "waveform":

            decoder.get("waveform")


        Returns
        -------
        Decoded value associated with the provided field name.
        """
        return self.fields[field_name]

    def get_api(self):
        """Utility method to return api field.

        Returns
        -------
        int
            API version of decoded file.
        """
        return self.get("rdvxApiVersion")

    def is_bar(self):
        """ Determines if this instance contains a barometer payload.

        Returns
        -------
        bool
            True if this instance contains a barometer payload, False otherwise.
        """
        return self.bin_format in bar_formats

    def is_mic(self):
        """ Determines if this instance contains a microphone payload.

        Returns
        -------
        bool
            True if this instance contains a microphone payload, False otherwise.
        """
        return self.bin_format in mic_formats

    def to_json(self):
        class RedvoxEncoder(json.JSONEncoder):
            def default(self, o):
                if isinstance(o, np.integer):
                    return int(o)
                elif isinstance(o, np.floating):
                    return float(o)
                elif isinstance(o, np.ndarray):
                    return o.tolist()
                else:
                    return super(RedvoxEncoder, self).default(o)

        return json.dumps(self.fields, cls=RedvoxEncoder)

    def get_field_names(self):
        return get_field_names(self.bin_format)

    def __str__(self):
        """
        Returns a string of this instance which will contain the key and value of each of its fields.

        Returns
        -------
        str
            A string of this instance which will contain the key and value of each of its fields.
        """
        string = ""
        for field_name in get_field_names(self.bin_format):
            string = string + field_name + ": " + str(self.fields[field_name]) + "\n"
        # If we're only reading the header, than the waveform field does not exist
        if not self.header_only:
            string = string + "waveform: " + str(self.fields["waveform"])
        return string

class DecodedRedvoxFileApi800(BinDecoder):
    """
    This class represents a thin-wrapper on top of a BinDecoder that maps to the wave format spec
    in a 1-to-1 manner.
    """
    def __init__(self, fname, bin_format, header_only=False, from_buffer=False, byte_buffer=None):
        BinDecoder.__init__(self, fname, bin_format, header_only=header_only, from_buffer=from_buffer, byte_buffer=byte_buffer)

    def headerChunkId(self):
        return self.get("headerChunkId")

    def headerChunkSize(self):
        return self.get("headerChunkSize")

    def headerChunkFormat(self):
        return self.get("headerChunkFormat")

    def headerSubChunkId(self):
        return self.get("headerSubChunkId")

    def headerSubChunkSize(self):
        return self.get("headerSubChunkSize")

    def headerAudioFormat(self):
        return self.get("headerAudioFormat")

    def headerNumChannels(self):
        return self.get("headerNumChannels")

    def headerSampleRate(self):
        return self.get("headerSampleRate")

    def headerByteRate(self):
        return self.get("headerByteRate")

    def headerBlockAlign(self):
        return self.get("headerBlockAlign")

    def headerBitsPerSample(self):
        return self.get("headerBitsPerSample")

    def rdvxChunkId(self):
        return self.get("rdvxChunkId")

    def rdvxChunkSize(self):
        return self.get("rdvxChunkSize")

    def rdvxApiVersion(self):
        return self.get("rdvxApiVersion")

    def rdvxServerRecvEpoch(self):
        return self.get("rdvxServerRecvEpoch")

    def rdvxIdForVendor(self):
        return self.get("rdvxIdForVendor")

    def rdvxTruncatedUuid(self):
        return self.get("rdvxTruncatedUuid")

    def rdvxLatitude(self):
        return self.get("rdvxLatitude")

    def rdvxLongitude(self):
        return self.get("rdvxLongitude")

    def rdvxAltitude(self):
        return self.get("rdvxAltitude")

    def rdvxSpeed(self):
        return self.get("rdvxSpeed")

    def rdvxHorizontalAccuracy(self):
        return self.get("rdvxHorizontalAccuracy")

    def rdvxVerticalAccuracy(self):
        return self.get("rdvxVerticalAccuracy")

    def rdvxCalibrationTrim(self):
        return self.get("rdvxCalibrationTrim")

    def rdvxTimeOfSolution(self):
        return self.get("rdvxTimeOfSolution")

    def rdvxFileStartMach(self):
        return self.get("rdvxFileStartMach")

    def rdvxFileStartEpoch(self):
        return self.get("rdvxFileStartEpoch")

    def rdvxNumMessageExchanges(self):
        return self.get("rdvxNumMessageExchanges")

    def rdvxMessageExchanges(self):
        return self.get("rdvxMessageExchanges")

    def rdvxSensorName(self):
        return self.get("rdvxSensorName")

    def rdvxDeviceName(self):
        return self.get("rdvxDeviceName")

    def rdvxSyncServer(self):
        return self.get("rdvxSyncServer")

    def rdvxDataServer(self):
        return self.get("rdvxDataServer")

    def dataChunkId(self):
        return self.get("dataChunkId")

    def dataChunkSize(self):
        return self.get("dataChunkSize")

    def waveform(self):
        return self.get("waveform")


def decode_mic_700(fname, header_only=False, from_buffer=False, byte_buffer=None):
    """Decodes an instance of API 700 microphone data from a binary payload either from disk of a byte buffer.

    Parameters
    ----------
    fname : str
        Filename of wave file to load and decode from disk. This can be empty or None if the file is instead loaded
        through a byte buffer.
    header_only : boolean, optional
        When True, only load header information (metadata) and skip loading of binary waveform
    from_buffer : boolean, optional
        When True, decode instance from byte buffer rather than from a file. Also when True, byte_buffer must not be
        None.
    byte_buffer : list[byte], optional
        When from_buffer is True, this field must contain a buffer of bytes that is a multiple in size of the dtype
        generated by passing in the selected bin_format.

    Returns
    -------
    BinDecoder
        A decoded instance of a API 700 microphone payload.
    """
    return BinDecoder(fname, mic_api_700, header_only, from_buffer, byte_buffer)


def decode_mic_800(fname, header_only=False, from_buffer=False, byte_buffer=None):
    """Decodes an instance of API 800 microphone data from a binary payload either from disk of a byte buffer.

    Parameters
    ----------
    fname : str
        Filename of wave file to load and decode from disk. This can be empty or None if the file is instead loaded
        through a byte buffer.
    header_only : boolean, optional
        When True, only load header information (metadata) and skip loading of binary waveform
    from_buffer : boolean, optional
        When True, decode instance from byte buffer rather than from a file. Also when True, byte_buffer must not be
        None.
    byte_buffer : list[byte], optional
        When from_buffer is True, this field must contain a buffer of bytes that is a multiple in size of the dtype
        generated by passing in the selected bin_format.

    Returns
    -------
    BinDecoder
        A decoded instance of a API 800 microphone payload.
    """
    # return BinDecoder(fname, mic_api_800, header_only, from_buffer, byte_buffer)
    return DecodedRedvoxFileApi800(fname, mic_api_800, header_only, from_buffer, byte_buffer)


def decode_bar_700(fname, header_only=False, from_buffer=False, byte_buffer=None):
    """Decodes an instance of API 700 barometer data from a binary payload either from disk of a byte buffer.

    Parameters
    ----------
    fname : str
        Filename of wave file to load and decode from disk. This can be empty or None if the file is instead loaded
        through a byte buffer.
    header_only : boolean, optional
        When True, only load header information (metadata) and skip loading of binary waveform
    from_buffer : boolean, optional
        When True, decode instance from byte buffer rather than from a file. Also when True, byte_buffer must not be
        None.
    byte_buffer : list[byte], optional
        When from_buffer is True, this field must contain a buffer of bytes that is a multiple in size of the dtype
        generated by passing in the selected bin_format.

    Returns
    -------
    BinDecoder
        A decoded instance of a API 700 barometer payload.
    """
    return BinDecoder(fname, bar_api_700, header_only, from_buffer, byte_buffer)


def decode_bar_800(fname, header_only=False, from_buffer=False, byte_buffer=None):
    """Decodes an instance of API 800 barometer data from a binary payload either from disk of a byte buffer.

    Parameters
    ----------
    fname : str
        Filename of wave file to load and decode from disk. This can be empty or None if the file is instead loaded
        through a byte buffer.
    header_only : boolean, optional
        When True, only load header information (metadata) and skip loading of binary waveform
    from_buffer : boolean, optional
        When True, decode instance from byte buffer rather than from a file. Also when True, byte_buffer must not be
        None.
    byte_buffer : list[byte], optional
        When from_buffer is True, this field must contain a buffer of bytes that is a multiple in size of the dtype
        generated by passing in the selected bin_format.

    Returns
    -------
    BinDecoder
        A decoded instance of a API 800 barometer payload.
    """
    # return BinDecoder(fname, bar_api_800, header_only, from_buffer, byte_buffer)
    return DecodedRedvoxFileApi800(fname, bar_api_800, header_only, from_buffer, byte_buffer)


def is_mic(fname):
    """Determines if a file contains a microphone payload based off the file name.

    This function looks for the presence of an 'H' or 'L' in the filename of the encoded binary file. If an 'H' or 'L'
    are present, this indicates that the file contains a microphone payload.

    Parameters
    ----------
    fname : str
        File name of the file to decode.

    Returns
    -------
    bool
        True if the encoded file contained a microphone payload, False otherwise.
    """
    fname = os.path.splitext(fname)[0]
    s = fname.split("/")[-1].split("_")
    t = s[3]
    return t == "H" or t == "L" or t == "A"


def is_bar(fname):
    """Determines if a file contains a barometer payload based off the file name.

    This function looks for the presence of a 'B' in the filename of the encoded binary file. If an 'B' is present,
    this indicates that the file contains a barometer payload.

    Parameters
    ----------
    fname : str
        File name of the file to decode.

    Returns
    -------
    bool
        True if the encoded file contained a barometer payload, False otherwise.
    """
    fname = os.path.splitext(fname)[0]
    s = fname.split("/")[-1].split("_")
    t = s[3]
    return t == "B"


def get_api(fname):
    """Extract the API from a file on disk.

    Parameters
    ----------
    fname : str
        File name of the file to extract API version from.

    Returns
    -------
    int
        API version of selected file.

    """
    with open(fname, 'rb') as f:
        f.seek(API_OFFSET)
        return struct.unpack("i", f.read(4))[0]


def get_api_from_buffer(byte_buffer):
    """Extract the API from a byte buffer.

    Parameters
    ----------
    byte_buffer : list[byte]
        List of bytes that make up encoded binary wave file.

    Returns
    -------
    int
        API version of byte buffer.
    """
    api_bytes = byte_buffer[API_OFFSET:API_OFFSET + API_WIDTH]
    return struct.unpack("i", api_bytes)[0]


# TODO: Merge with decode_buffer
def decode(fname, header_only=False, tmp_filename=None):
    """Helper function that automatically determines API and decodes a binary payload from disk.

    Parameters
    ----------
    fname : str
        File name to load and decode from disk.
    header_only : bool, optional
        When True, only load wave header information (metadata) and skip loading binary waveform.
    tmp_filename : str, optional
        This is deprecated and should remain None. This will most likely be removed in further updates.
    Returns
    -------
    BinDecoder
        An instance of a decoded file from disk.
    """
    f = fname if tmp_filename is None else tmp_filename

    ismic = is_mic(fname)
    isbar = is_bar(fname)
    if not ismic and not isbar:
        print fname, "is not a mic or bar file"
        return

    api = get_api(f)
    if api not in valid_apis:
        print f, api, "is not a valid api version"
        return

    if ismic:
        if api == 0x700:
            return decode_mic_700(f, header_only)
        elif api == 0x800:
            return decode_mic_800(f, header_only)
        else:
            print fname, api, "not valid"
            return
    elif isbar:
        if api == 0x700:
            return decode_bar_700(f, header_only)
        elif api == 0x800:
            return decode_bar_800(f, header_only)
        else:
            print f, api, "not valid"
            return
    else:
        print f, api, "not valid"
        return


# TODO: Merge with decode.
def decode_buffer(fname, byte_buffer, header_only=False):
    """Helper function that automatically determines API and decodes a binary payload from a byte buffer.

    Parameters
    ----------
    fname : str
        S3 key or file name of byte buffer source.
    byte_buffer : list[byte]
        List of bytes or buffer like object that makes up encoded binary payload.
    header_only : bool, optional
        When True, only load wave header information (metadata) and skip loading binary waveform.
    Returns
    -------
    BinDecoder
        An instance of a decoded file from a byte buffer.
    """
    ismic = is_mic(fname)
    isbar = is_bar(fname)
    if not ismic and not isbar:
        print fname, "is not a mic or bar file"
        return

    api = get_api_from_buffer(byte_buffer)
    if api not in valid_apis:
        print fname, api, "is not a valid api version"
        return

    if ismic:
        if api == 0x700:
            return decode_mic_700(fname, header_only, from_buffer=True, byte_buffer=byte_buffer)
        elif api == 0x800:
            return decode_mic_800(fname, header_only, from_buffer=True, byte_buffer=byte_buffer)
        else:
            print fname, api, "not valid"
            return
    elif isbar:
        if api == 0x700:
            return decode_bar_700(fname, header_only, from_buffer=True, byte_buffer=byte_buffer)
        elif api == 0x800:
            return decode_bar_800(fname, header_only, from_buffer=True, byte_buffer=byte_buffer)
        else:
            print fname, api, "not valid"
            return
    else:
        print fname, api, "not valid"
        return

if __name__ == "__main__":
    import sys
    redvox_files = sys.argv[1:]

    if len(redvox_files) > 0:
        for redvox_file in redvox_files:
            print decode(fname=redvox_file)