#!/usr/bin/env python

import BaseHTTPServer
import glob
import json
import os.path
import os
import sys

import redvox_reader_standalone as reader

if len(sys.argv) <= 1:
    print "Please supply the base directory as the only command line parameter"
    exit()

base_dir = sys.argv[1]

HOST = ""
PORT = 8001
CD_ENABLED = True

INDEX = """<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Redvox File Explorer</title>
    <link rel="stylesheet" href="https://unpkg.com/purecss@0.6.0/build/pure-min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dygraph/1.1.1/dygraph-combined.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="margin-left: 10px; margin-right: 10px">
<div class="pure-g">
    <div class="pure-u-1-1">
        <br/>
        <center><b>Redvox File Explorer</b></center>
        <hr/>
    </div>
</div>

<div class="pure-g">
    <div id="dir" class="pure-u-2-5">
        <p>Available Directories</p>
        <table id="dir-table" class="pure-table">
            <thead>
            <tr>
                <th id="path">/ <i class="fa fa-refresh" aria-hidden="true" style="float: right; padding-left: 10px"></i></th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
        <br />
        <p>Available Files</p>
        <table  id="wave-table" class="pure-table">
            <thead>
            <tr>
                <th>Time</th>
                <th>Device</th>
                <th>H|L|B</th>
                <th>Msg #</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

    <div id="details" class="pure-u-3-5">
        <p>Waveform</p>
        <div id="waveform-plot" style="width: auto"></div>
        <br />
        <p>Details</p>
        <table id="details-table" class="pure-table" style="width: 100%">
            <thead>
            <tr>
                <th>Field</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<script type="text/javascript">
    function getTime(epochSec) {
        var asInt = parseInt(epochSec);
        var time = new Date(0);
        time.setUTCSeconds(asInt);
        return time.getUTCFullYear() + "/" + (time.getUTCMonth() + 1) + "/" + time.getUTCDate() + " " + time.getUTCHours() + ":" + time.getUTCMinutes() + ":" + time.getUTCSeconds()
    }

    function getDirectoryListing(path) {
        // Delete old tables first
        $("#dir-table").find("tbody").empty();
        $("#wave-table").find("tbody").empty();
        $("#details-table").find("tbody").empty();
        $("#waveform-plot").empty();

        var spath = path.split("/");
        if(spath[spath.length - 1] == "..") {
            path = spath.splice(0, spath.length - 2).join("/");
        }

        $("#path")[0].innerHTML = "/" + path + '<i class="fa fa-refresh" aria-hidden="true" style="float: right; padding-left: 10px"></i>';

        console.log("GET api/dir/" + path);
        $.get("api/dir/" + path, function (data) {
            var dirs = data["dirs"].sort();
            for(var s = 0; s < dirs.length; s++) {
                var full_dir = dirs[s];
                var sdir = full_dir.split("/");
                var dir = sdir[sdir.length - 1];
                $("#dir-table").find("tbody").append("<tr id='" +  full_dir + "'><td>" + dir + "</td></tr>")
            }

            var waves = data["waves"].sort(function(a, b) {
                return parseInt(b.split("_")[2]) - parseInt(a.split("_")[2]);
            });

            for (var i = 0; i < waves.length; i++) {
                var split = waves[i].split("_");
                var deviceId = split[1];
                var timestamp = getTime(split[2]);
                var hlb = split[3];
                var msgs = split[4].substring(0, 2);
                var row = "<tr id='" + waves[i] + "'>" +
                    "<td>" + timestamp + "</td>" +
                    "<td>" + deviceId + "</td>" +
                    "<td>" + hlb + "</td>" +
                    "<td>" + msgs + "</td>" +
                    "</tr>";
                $("#wave-table").find("tbody").append(row);
            }
        });
    }

    function getDetails(fileId) {
        var fieldNames = ['headerChunkId', 'headerChunkSize', 'headerChunkFormat', 'headerSubChunkId',
            'headerSubChunkSize', 'headerAudioFormat', 'headerNumChannels', 'headerSampleRate', 'headerByteRate',
            'headerBlockAlign', 'headerBitsPerSample', 'rdvxChunkId', 'rdvxChunkSize', 'rdvxApiVersion',
            'rdvxServerRecvEpoch', 'rdvxIdForVendor', 'rdvxTruncatedUuid', 'rdvxLatitude', 'rdvxLongitude',
            'rdvxAltitude', 'rdvxSpeed', 'rdvxHorizontalAccuracy', 'rdvxVerticalAccuracy', 'rdvxCalibrationTrim',
            'rdvxTimeOfSolution', 'rdvxFileStartMach', 'rdvxFileStartEpoch', 'rdvxNumMessageExchanges',
            'rdvxSensorName', 'rdvxDeviceName', 'rdvxSyncServer', 'rdvxDataServer',
            'dataChunkId', 'dataChunkSize'];//, "waveform"];

        $("#details-table").find("tbody").empty();

        console.log("GET api/details/" + fileId);
        $.get("api/details/" + fileId, function (data) {
            for(var i = 0; i < fieldNames.length; i++) {
                var row = "<tr><td>" + fieldNames[i] + "</td><td>" + data[fieldNames[i]] + "</td></tr>";
                $("#details-table").find("tbody").append(row);
            }


            // Add in message exchange expando
            $("#details-table").find("tbody").append("<tr><td>rdvxMessageExchanges</td><td id='msgs-td'>Click to Expand</td></tr>");
            $("#msgs-td").on("click", function(_data) {
                if(_data.target.innerHTML == "Click to Expand") {
                    _data.target.innerHTML = data["rdvxMessageExchanges"].toString();
                }
                else {
                    _data.target.innerHTML = "Click to Expand";
                }

            });

            // Add in waveform expando
            $("#details-table").find("tbody").append("<tr><td>Waveform</td><td id='waveform-td'>Click to Expand</td></tr>");
            $("#waveform-td").on("click", function(_data) {
                if(_data.target.innerHTML == "Click to Expand") {
                    _data.target.innerHTML = data["waveform"].toString();
                }
                else {
                    _data.target.innerHTML = "Click to Expand";
                }
            });

            // Display plot
            var waveform = data["waveform"];
            var plotData = [];
            var sensorType = data["headerChunkFormat"].trim();

            if(sensorType == "WAVE") {
                var sampleRate = data["headerSampleRate"];
                var startTimestampMs = data["rdvxFileStartEpoch"] / 1000.0;
                for(var j = 0; j < waveform.length; j++) {
                    var date = new Date(startTimestampMs + (j * ((1.0 / sampleRate) * 1000.0)));
                    plotData.push([date, waveform[j]]);
                }
                new Dygraph(document.getElementById("waveform-plot"), plotData,
                    {
                        labelsUTC: true,
                        labels: ['UTC', 'Counts']
                    });
            }

            if(sensorType == "BAR") { // http://pinkstone.co.uk/how-to-access-the-barometer-in-ios-8/ // but it looks like its in milliseconds here?
                var startTimestampMs = data["rdvxFileStartEpoch"] / 1000.0;
                var sampleDeltaMs = waveform[0];
                for(var k = 1; k < waveform.length; k += 2) {
                    var date = new Date(startTimestampMs + (waveform[k - 1] - sampleDeltaMs));
                    plotData.push([date, waveform[k]])
                }
                new Dygraph(document.getElementById("waveform-plot"), plotData,
                    {
                        labelsUTC: true,
                        labels: ['UTC', 'Pressure']
                    });
            }
        });
    }

    $(document).ready(function () {
        getDirectoryListing("");

        $("#dir-table").find("thead").delegate("tr", "click", function(data) {
            var pathId = $("#path")[0].innerHTML.substring(1, $("#path")[0].innerHTML.length - '<i class="fa fa-refresh" aria-hidden="true" style="float: right; padding-left: 10px"></i>'.length);
            getDirectoryListing(pathId);
        });

        $("#dir-table").find("tbody").delegate("tr", "click", function(data) {
            var pathId = data.currentTarget.id;
            getDirectoryListing(pathId);
        });

        $("#wave-table").find("tbody").delegate("tr", "click", function(data) {
           var fileId = data.currentTarget.id;
           getDetails(fileId);
        });
    });
</script>

</body>
</html>"""

class ServerHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, request, client_address, server)

    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()

    def do_GET(self):
        self.route(self.path)

    def route(self, path):
        if path == "/":
            self.resp_html(self.index())
        elif path.startswith("/api/dir"):
            self.resp_json(self.dir(path))
        elif path.startswith("/api/details"):
            self.resp_json(self.details(path))
        else:
            self.resp_404()

    def resp_html(self, content):
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(content)

    def resp_json(self, content):
        self.send_response(200)
        self.send_header("Content-type", "application/json")
        self.end_headers()
        self.wfile.write(content)

    def resp_404(self):
        return self.resp_err(404)

    def resp_500(self):
        return self.resp_err(500)

    def resp_err(self, code):
        self.resp_html("<h1>{}</h1>".format(code))

    def index(self):
        with open("server.html", "r") as f:
            return f.read()

    # def index(self):
    #     return INDEX

    def dir(self, full_path):
        if not CD_ENABLED:
            waves = glob.glob1(base_dir, "*.wav")
            return json.dumps({"dirs":[], "waves": waves})

        if full_path == "/api/dir" or full_path == "/api/dir/":
            path = None
        else:
            path = full_path[len("/api/dir/"):]

        if path is None:
            _dir = base_dir
        else:
            _dir = base_dir + "/" + path

        # dirs = filter(os.path.isdir, next(os.walk(_dir))[1])
        dirs = filter(lambda x: os.path.isdir(_dir + "/" + x), next(os.walk(_dir))[1])
        if path is not None:
            dirs = map(lambda x: path + "/" + x, dirs)
            dirs.insert(0, path + "/" + "..")

        for x in next(os.walk(_dir))[1]:
            print x, os.path.isdir(_dir + "/" + x)

        print _dir, dirs, next(os.walk(_dir))[1]
        waves = glob.glob1(_dir, "*.wav")
        if path is not None:
            waves = map(lambda w: path + "/" + w, waves)

        result = {"dirs": dirs,
                  "waves": waves}

        return json.dumps(result)

    def details(self, path):
        if not CD_ENABLED:
            file_name = path.split("/")[-1]
            file_path = base_dir + "/" + file_name
            if os.path.exists(file_path):
                return reader.decode(file_path).to_json()
            else:
                return json.dumps({"error": "resource not found"})

        if path.startswith("/api/details/"):
            file_path = base_dir + "/" + path[len("/api/details/"):]

        if os.path.exists(file_path):
            return reader.decode(file_path).to_json()
        else:
            return json.dumps({"error": "resource not found"})


print "Starting server at http://{}:{}".format(HOST, PORT)
server = BaseHTTPServer.HTTPServer((HOST, PORT), ServerHandler)

try:
    server.serve_forever()
except KeyboardInterrupt:
    pass

server.server_close()
